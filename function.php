<?php

function siteUrl($uri = '')
{
    return BASE_URL . $uri;
}

function getUser()
{
    $users = json_decode(file_get_contents(USER_DB_PATH), 1);
    return $users;
}


function addUser($user)
{
    $users = getUser();
    $users[] = $user;
    $users_json = json_encode($users);
    file_put_contents(USER_DB_PATH, $users_json);
}

function back($message)
{ 
    echo "<script>alert('$message')</script>";
    echo "<script>window.open('../index.php','_self')</script>";
}
