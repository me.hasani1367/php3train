<?php

// lets start with a simple example 
class Model
{
    protected static $tableName = 'Model';
    public static function getTableName()
    {
        return self::$tableName;
    }
}
class User extends Model
{
    protected static $tableName = 'User';
}
echo User::getTableName();


// we used self and the operator :: to access static property indide 
// the Model class.

// self always resolved to the class in which the Method belongs.
// it means if you define a method in a parent class and call it 
// from a subclass, the self does not reference to the subclass as we expect.

// for overcomig the issue
// php 5.3 introduced a new feature called PHP static late binding
// instead of using the self, you use the static keyword that reference 
// to the exact class that we called at runtime.

// lets modify our example above:
// uncomment bellow code to see the result


// class Model
// {
//     protected static $tableName = 'Model';
//     public static function getTableName()
//     {
//         return static::$tableName;
//     }
// }

// class User extends Model
// {
//     protected static $tableName = 'User';
// }

// echo User::getTableName();
