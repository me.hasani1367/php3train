<?php

include "../init.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['submit'])) {
        if (!empty($_POST['user_name']) && (!empty($_POST['user_password']))) {

            $userName = $_POST['user_name'];
            $userPassword = $_POST['user_password'];
            $users = getUser();
            foreach ($users as $user) {
                // var_dump($user);
                if ($userName == $user['name'] && $userPassword == $user['password']) {
                    $_SESSION['islog']=true;
                    back('you are sign in.');
                } 
            }
            back('wrong information try again.');
        } else {
            back('enter complete information');
        }
    }
}
