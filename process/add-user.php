<?php

include "../init.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['register'])) {
        if (!empty($_POST['user_name']) && (!empty($_POST['user_password']))) {

            $userName = $_POST['user_name'];
            $userPassword = $_POST['user_password'];
            $new_user = array(
                'name' => $userName,
                'password' => $userPassword
            );
            $users = getUser();
            if ($users != null) {
                foreach ($users as $user) {
                    // var_dump($user);
                    if ($userName == $user['name']) {
                        $error = "already exist. please enter another name.";
                        header('Location:../index.php?error=' . $error . '');
                        die();
                    }
                }
            }
            addUser($new_user);
            back('you are registerd seccessfully.');
        } else {
            back('enter complete information');
        }
    }
}
